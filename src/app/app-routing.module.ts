import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RegistrationComponent } from './registration/registration.component';
import { FilemanagerComponent } from './filemanager/filemanager.component';
import { RecentComponent } from './recent/recent.component';
import { TrashComponent } from './trash/trash.component';

const routes: Routes = [
  { path: 'registration', component: RegistrationComponent },
  { path: 'filemanager', component: FilemanagerComponent },
  { path: 'recent', component: RecentComponent },
  { path: 'trash', component: TrashComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
