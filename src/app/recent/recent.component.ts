import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-recent',
  templateUrl: './recent.component.html',
  styleUrls: ['./recent.component.css']
})
export class RecentComponent implements OnInit {

  file_cards = [
    {
      'title': 'Shared Files',
      'description': '3 items, 17 September, 2021',
    },
    {
      'title': 'Media',
      'description': '2 items, 18 September, 2021',
    },
    {
      'title': 'Gallery',
      'description': '8 items, 18 September, 2021',
    },
    {
      'title': 'Documents',
      'description': '10 items, 18 September, 2021',
    },
    {
      'title': 'Important',
      'description': '2 items, 20 September, 2021',
    }
  ];
  constructor() { }

  ngOnInit(): void {
  }

}
