import { Component } from '@angular/core';
import { map } from 'rxjs/operators';
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';

@Component({
  selector: 'app-filemanager',
  templateUrl: './filemanager.component.html',
  styleUrls: ['./filemanager.component.css'],
})
export class FilemanagerComponent {
  /** Based on the screen size, switch from standard to one column per row */
  cards = this.breakpointObserver.observe(Breakpoints.Handset).pipe(
    map(({ matches }) => {
      if (matches) {
        return [
          { title: 'Card 1', cols: 1, rows: 1 },
          { title: 'Card 2', cols: 1, rows: 1 },
          { title: 'Card 3', cols: 1, rows: 1 },
          { title: 'Card 4', cols: 1, rows: 1 },
        ];
      }

      return [
        { title: 'Card 1', cols: 2, rows: 1 },
        { title: 'Card 2', cols: 1, rows: 1 },
        { title: 'Card 3', cols: 1, rows: 2 },
        { title: 'Card 4', cols: 1, rows: 1 },
      ];
    })
  );

  file_cards = [
    {
      title: 'Shared Files',
      description: '3 items, 17 September, 2021',
    },
    {
      title: 'Media',
      description: '2 items, 18 September, 2021',
    },
    {
      title: 'Gallery',
      description: '8 items, 18 September, 2021',
    },
    {
      title: 'Documents',
      description: '10 items, 18 September, 2021',
    },
    {
      title: 'Important',
      description: '2 items, 20 September, 2021',
    },
  ];

  constructor(private breakpointObserver: BreakpointObserver) {}
}
