import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  isStorageToggled: boolean = false;
  isTeamToggled: boolean = false;
  isAnalyticsToggled: boolean = false;
  isUserToggled: boolean = false;
  isSettingsToggled: boolean = false;

  storageExp: boolean = false;
  teamExp: boolean = false;
  analyticsExp: boolean = false;
  userExp: boolean = false;
  settingsExp: boolean = false;
  constructor() { }

  ngOnInit(): void {
    
  }

  changeTextColor = (id: any) => {
    (<HTMLElement>document.getElementById(id)).style.color = '#FCBF38';
  }

  

  
}
